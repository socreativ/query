<?php 

function acf_load_postype_choices( $field ) {
    $post_types = get_post_types(
        ['publicly_queryable' => true],
        'names'
    );
    $choices = [];
    foreach ($post_types as $post_type) {
        $choices[$post_type] = $post_type; 
    }
    $field['choices'] = $choices;
    return $field;
}
add_filter('acf/load_field/key=field_610be6353bd8b', 'acf_load_postype_choices');

?>