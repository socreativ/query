<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$tax_args = array();
$meta_args = array();


if(get_field('is_taxo')){

    if(count(get_field('taxonomies')) > 1){
        $tax_args['relation'] = strtoupper(get_field('tax_relation'));
    }

   if(have_rows('taxonomies')){
       while(have_rows('taxonomies')){
           the_row();
           $arr = array(
               'taxonomy'   => get_sub_field('taxonomy'),
               'field'      => get_sub_field('field'),
               'terms'      => get_sub_field('terms'),
               'operator'   => get_sub_field('operator')
           );
           array_push($tax_args, $arr);
       }
   }

}

if(get_field('is_meta')){

    if(count(get_field('meta')) > 1){
        $meta_args['relation'] = strtoupper(get_field('meta_relation'));
    }

    if(have_rows('meta')){
        while(have_rows('meta')){
            the_row();
            $arr = array(
                'meta_key'      => get_sub_field('key'),
                'meta_value'    => get_sub_field('value'),
                'meta_compare'  => get_sub_field('compare')
            );
            array_push($tax_args, $arr);
        }
    }

}

$args = array(
    'post_type'     => get_field('post_type'),
    'post_status'   => 'publish',
    'posts_per_page' => get_field('ppp'),
    'tax_query'     => $tax_args,
    'meta_query'    => $meta_args,
    'order_by'      => get_field('order_by'),
    'order'         => get_field('order')
);


if(get_field('is_category')){
    $args[get_field('cat_type')] = get_field('cat_value');
}

$query = new WP_Query($args);
$columns = my_wp_is_mobile() ? 2 : get_field('columns');

?>



<div id="<?= $id ?>" class="wp-block-query <?= $css ?>">
    <div style="display:grid;grid-template-columns: <?php for($i=0;$i<$columns;$i++) echo 'auto ' ?>;">
        <?php if($query->have_posts()): 
            while($query->have_posts()): 
                $query->the_post(); 
                get_template_part(get_field('template'));
            endwhile;
        endif; ?>
    </div>
</div>